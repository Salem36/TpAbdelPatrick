import { Component, OnInit } from '@angular/core';
import { Produit } from '../../class/produit';


@Component({
  selector: 'app-tableau',
  templateUrl: './tableau.component.html',
  styleUrls: ['./tableau.component.css']
})
export class TableauComponent implements OnInit {
  listeProduit: Array<Produit> = new Array<Produit>();
  p: Produit = new Produit();
  isShow :boolean =false;

  constructor() {
    this.p.nom = "peugeot";
    this.p.code = "1544-pg-206";
    this.p.evaluation = 50;
    this.p.datePeremption = new Date('2017/09/15');
    this.p.prix = 19.50;
    this.p.lien = "../../assets/img/Peugeot.png";
    this.listeProduit.push(this.p);

    let p1: Produit = new Produit();
    p1.nom = "Renault";
    p1.code = "1200-rn-Clio";
    p1.evaluation = 30;
    p1.datePeremption = new Date('2016/05/02');
    p1.prix = 22.10;
    p1.lien = "../../assets/img/renault.png";
    this.listeProduit.push(p1);

    let p2: Produit = new Produit();
    p2.nom = "Citroen";
    p2.code = "5647-ct-C4";
    p2.evaluation = 100;
    p2.datePeremption = new Date('2015/08/14');
    p2.prix = 85.00;
    p2.lien = "../../assets/img/citroen.png";
    this.listeProduit.push(p2);

  }
  AfficherImage(){
    this.isShow=!this.isShow;
  }

  ngOnInit() {
  }

}
